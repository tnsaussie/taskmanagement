package com.tns.TNSTaskMgmt.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.tns.TNSTaskMgmt.dao.CreateTaskDAO;
import com.tns.TNSTaskMgmt.dao.UserRegistrationDAO;
import com.tns.TNSTaskMgmt.entities.Task;
import com.tns.TNSTaskMgmt.entities.User;

@Controller
public class HomeController {

	@RequestMapping(value="/registerUser")
	public ModelAndView test(HttpServletResponse response,HttpServletRequest request) throws IOException{
		ApplicationContext appContext1=new ClassPathXmlApplicationContext("spring-configuration.xml");
		UserRegistrationDAO userDao = (UserRegistrationDAO) appContext1.getBean("userdao");
		User user=new User();
		
		
		user.setFirstname(request.getParameter("first_name"));
		user.setLastname(request.getParameter("last_name"));
		user.setUsername(request.getParameter("username"));
		user.setPassword(request.getParameter("password"));
		user.setEmail(request.getParameter("email"));
		userDao.addUser(user);
		
		return new ModelAndView("success","message",user.getUsername());
	}
	
	@RequestMapping(value="/AssignRole")
	public ModelAndView test2(HttpServletResponse response,HttpServletRequest request) throws IOException{
		
		
		return new ModelAndView("CreateUser");
	}
	@RequestMapping(value="/createUser")
	public String test3(HttpServletResponse response,HttpServletRequest request) throws IOException{
		ApplicationContext appContext1=new ClassPathXmlApplicationContext("spring-configuration.xml");
		UserRegistrationDAO userDao = (UserRegistrationDAO) appContext1.getBean("userdao");
		User user=new User();
		user.setUsername(request.getParameter("username"));
		String email = userDao.createUser(user);
		System.out.println("email is" + email);
		request.setAttribute("emailId",email);
		request.setAttribute("username", user.getUsername());
		
		return "CreateUser";
	}
	
	@RequestMapping(value="/createTask")
	public ModelAndView createTask(HttpServletResponse response,HttpServletRequest request) throws IOException{
		ApplicationContext appContext1=new ClassPathXmlApplicationContext("spring-configuration.xml");
		CreateTaskDAO TaskDao = (CreateTaskDAO) appContext1.getBean("taskdao");
		Task task=new Task();
		task.setTaskName(request.getParameter("taskname"));
		task.setTaskDesc(request.getParameter("taskdesc"));
		TaskDao.createTask(task);
		
		return new ModelAndView("success","message",task.getTaskName());
	}
	
	@RequestMapping(value="/")
	public ModelAndView test1(HttpServletResponse response,HttpServletRequest request) throws IOException{
		
		
		return new ModelAndView("home");
	}
	@RequestMapping(value="/createtask")
	public ModelAndView createtaskredirect(HttpServletResponse response,HttpServletRequest request) throws IOException{
		
		
		return new ModelAndView("createTask");
	}
	@RequestMapping(value="/index", method = RequestMethod.GET)  
	 public String executeSecurity(Model model) {  
	   
	  model.addAttribute("message", "Spring Security Hello World");  
	  model.addAttribute("author", "By TNS");  
	  return "welcome";  
	   
	 } 
	@RequestMapping(value="/updateTask")
	public ModelAndView updateTask(HttpServletResponse response,HttpServletRequest request) throws IOException
	{
	//fetch the task from the data base code goes here.	
	return new ModelAndView("updateTask");
	}
	
}

