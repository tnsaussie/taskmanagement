package com.tns.TNSTaskMgmt.dao.impl;
import java.util.Iterator;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.tns.TNSTaskMgmt.dao.UserRegistrationDAO;
import com.tns.TNSTaskMgmt.entities.User;
public class UserRegistrationDAOImpl implements UserRegistrationDAO {

	User user;
	SessionFactory sessionFactory;
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	@Override
	public void addUser(User user) {
	Session session=sessionFactory.openSession();
	session.beginTransaction();	
	session.save(user);
    session.getTransaction().commit();
    session.close();
	
	}
	
	@Override
	public String createUser(User user) {
	Session session=sessionFactory.openSession();
	Criteria crit = session.createCriteria(User.class);
	//Criteria crit = session.createCriteria(EForm.class);
	User userDet = null;
	if (user.getUsername() != null) {
		crit.add(Restrictions.like("username", user.getUsername()));
	
	Iterator<User> iterator = crit.list().iterator();
	if (iterator.hasNext()) {
		userDet = (User)iterator.next();
		return userDet.getEmail();
	}
	}
	
	return null;
	}
}
