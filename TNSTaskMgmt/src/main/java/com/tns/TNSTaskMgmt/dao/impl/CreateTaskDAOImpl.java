package com.tns.TNSTaskMgmt.dao.impl;
import java.util.Iterator;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import com.tns.TNSTaskMgmt.dao.CreateTaskDAO;
import com.tns.TNSTaskMgmt.entities.Task;
	
	public class CreateTaskDAOImpl implements CreateTaskDAO {

		Task task;
		SessionFactory sessionFactory;
		public SessionFactory getSessionFactory() {
			return sessionFactory;
		}
		public void setSessionFactory(SessionFactory sessionFactory) {
			this.sessionFactory = sessionFactory;
		}
		@Override
		public void createTask(Task task) {
		Session session=sessionFactory.openSession();
		session.beginTransaction();	
		session.save(task);
	    session.getTransaction().commit();
	    session.close();
		
		}
		
		
	}

	
	

