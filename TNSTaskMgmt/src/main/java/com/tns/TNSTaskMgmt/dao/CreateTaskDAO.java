package com.tns.TNSTaskMgmt.dao;
import com.tns.TNSTaskMgmt.entities.Task;
public interface CreateTaskDAO {
public void createTask(Task task);
}