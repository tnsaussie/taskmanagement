package com.tns.TNSTaskMgmt.dao;

import com.tns.TNSTaskMgmt.entities.User;

public interface UserRegistrationDAO {
	public void addUser(User user);
	
	public String createUser(User user);
}
