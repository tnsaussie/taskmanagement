/**
 * 
 */
package com.tns.TNSTaskMgmt.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author rajesh.rajamanickam
 * Task entity for create task and update task module
 *
 */
@Entity
@Table(name = "tnsdemo.task")
public class Task
{   
	@Id @GeneratedValue
	@Column(name = "task_id")
	int taskid;
	@Column(name = "task_name")
	private String taskName;
	@Column(name = "task_desc")
	private String taskDesc;
	public int getTaskid() {
		return taskid;
	}
	public void setTaskid(int taskid) {
		this.taskid = taskid;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getTaskDesc() {
		return taskDesc;
	}
	public void setTaskDesc(String taskDesc) {
		this.taskDesc = taskDesc;
	}
	

}
