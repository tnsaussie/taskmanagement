<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">
 <%@ include file="../../resources/css/register.css" %>
 </style>
<title>TNS TimeSheet - User Registration</title>
<script type="text/javascript" src="././resources/js/form-validation.js"></script> 
</head>
<body onload="firstfocus();">
<div id="wrapper">
<form name ="createTask" action="/TNSTaskMgmt/createTask" onsubmit="return validateForm()">
 <fieldset>
 <legend>Create Task</legend>
<div>
<label for="taskname">Task Name</label>
</div>
<div>
<input id="taskname" type="text" name="taskname" maxlength="10" onblur="validate_taskname();" />
</div>
<div>
<label for="taskdesc">Task Description</label>
</div>
<div>
<input id="taskdesc" type="text" name="taskdesc" onblur="Validate_taskdesc();" />
</div>
<div>
<input type="submit" name="submit" value="Create Task"/>
</div>
</fieldset>
</form>
</div>

</body>
</html>