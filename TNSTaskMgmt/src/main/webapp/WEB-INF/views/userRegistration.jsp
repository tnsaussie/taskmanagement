<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">
 <%@ include file="../../resources/css/register.css" %>
 </style>
<title>TNS TimeSheet - User Registration</title>
<script type="text/javascript" src="././resources/js/form-validation.js"></script> 
</head>
<body onload="firstfocus();">
<div id="wrapper">
<form name ="UserRegistration" action="/TNSTaskMgmt/registerUser" onsubmit="return validateForm()">
 <fieldset>
 <legend>User Registration</legend>
 <div><label for="first_name">First name </label></div>
<div>
<input name="first_name" maxlength="15" type="text" id="first_name" />
</div>
<div>
<label for="last_name">Last name </label>
</div>
<div>
<input name="last_name" maxlength="15" type="text" id="last_name" />
</div>
<div>
<label for="user_name">Username</label>
</div>
<div>
<input id="username" type="text" name="username" maxlength="10" onblur="validate_username();" />
</div>
<div>
<label for="password">Password</label>
</div>
<div>
<input id="password" type="password" name="password" maxlength="15" onblur="validate_password();" />
</div>
<div>
<label for="email">Email</label>
</div>
<div>
<input id="email" type="text" name="email" onblur="ValidateEmail();" />
</div>
<div>
<input type="submit" name="submit" value="Register"/>
</div>
</fieldset>
</form>
</div>

</body>
</html>