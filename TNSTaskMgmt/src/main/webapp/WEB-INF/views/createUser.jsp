<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">
 <%@ include file="../../resources/css/register.css" %>
 </style>
<title>TNS TimeSheet - User Registration</title>
<script type="text/javascript" src="././resources/js/form-validation.js"></script> 
</head>
<body onload="firstfocus();">
<div id="wrapper">

<form name ="CreateUser" action="/TNSTaskMgmt/createUser" method="post" onsubmit="return validateUserForm()">
 <fieldset>
 <legend>Create User</legend>
 

<div>
<label for="user_name">Username</label>
</div>
<div>
<input id="username" type="text" name="username" maxlength="10" value='${username}' />
</div>
<div>
<label for="email">Email</label>
</div>
<div>
<input id="email" type="text" name="email" value='${emailId}' />
</div>
<div>
<label for="role">Roles</label>
</div>
<div>
<select>
   <option value="default">Default</option>
   <option value="manager">Manager</option>
   <option value="admin">Administrator</option>
 </select>
</div>
<div>
<input type="submit" name="submit" value="Fetch User" onclick=""/>
</div>
<div>
<input type="submit" name="submit" value="Assign Role" onclick=""/>
</div>
<input name=action type=hidden>
</fieldset>
</form>
</div>

</body>
</html>