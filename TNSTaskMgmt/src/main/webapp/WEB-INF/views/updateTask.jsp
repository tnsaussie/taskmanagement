<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">
 <%@ include file="../../resources/css/register.css" %>
 </style>
<title>TNS TimeSheet - Update Task</title>
<script type="text/javascript" src="././resources/js/form-validation.js"></script> 
</head>
<body onload="">
<div id="wrapper">
	<form name ="updateTask" action="" method="post" onsubmit="">
	 <fieldset>
	 <legend>Update Task</legend>
	<div>
	<label for="updateTaskSelect">Tasks:</label>
	</div>
	<div>
	<select>
	   
	 </select>
	</div>
		<div>
		<label for="task_name">Task Name</label>
		</div>
		<div>
		<input  type="text" name="taskname" maxlength="10" />
		</div>
		<div>
		<label for="task_name">Task Description</label>
		</div>
		<div>
		<input  type="text" name="taskdesc" maxlength="30"/>
		</div>
		<div>
		<input type="submit" name="submit" value="Update Task"/>
		</div>
		
	</fieldset>
	</form>
</div>
</body>
</html>